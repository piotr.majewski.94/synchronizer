package pl.pmajewski.synchronizer.evernote.client;

import com.evernote.auth.EvernoteAuth;
import com.evernote.auth.EvernoteService;
import com.evernote.clients.ClientFactory;
import com.evernote.clients.NoteStoreClient;
import com.evernote.edam.error.EDAMNotFoundException;
import com.evernote.edam.error.EDAMSystemException;
import com.evernote.edam.error.EDAMUserException;
import com.evernote.edam.notestore.NoteFilter;
import com.evernote.edam.type.Note;
import com.evernote.edam.type.Notebook;
import com.evernote.thrift.TException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@SpringBootTest
class EvernotePreconditionsTest {
    private static final Logger log = LoggerFactory.getLogger(EvernotePreconditionsTest.class);
    private static final String DEFAULT_NOTEBOOK = "Inbox";
    private static final String REMOTE_NOTEBOOK_PREFIX = "REMOTE_NOTEBOOK_";
    private static final String REMOTE_NOTE_TITLE_PREFIX = "REMOTE_NOTE_";
    private static final String REMOTE_CONTENT_PREFIX = "REMOTE_CONTENT_";
    private static final String DEFAULT_NOTE_CONTENT = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!DOCTYPE en-note SYSTEM \"http://xml.evernote.com/pub/enml2.dtd\">\n<en-note>${noteBody}</en-note>";
    private static final String REMOTE_STACK_PREFIX = "REMOTE_STACK_";

    @Value("${evernote.token}")
    private String token;

    private static EvernoteAuth evernoteAuth;
    private static ClientFactory clientFactory;
    private static NoteStoreClient noteStore;

    @BeforeEach
    void beforeAll() throws TException, EDAMUserException, EDAMSystemException {
        if (evernoteAuth == null) {
            evernoteAuth = new EvernoteAuth(EvernoteService.SANDBOX, token);
        }

        if (clientFactory == null) {
            clientFactory = new ClientFactory(evernoteAuth);
        }

        if (noteStore == null) {
            noteStore = clientFactory.createNoteStoreClient();
        }
    }

    @Test
    void listNotebooks() throws TException, EDAMUserException, EDAMSystemException, EDAMNotFoundException {
        log.info("@listNotebooks");
        getNotebooks().stream()
                .forEach(n -> System.out.println("\t" + n));
    }

    private List<Notebook> getNotebooks() throws EDAMUserException, EDAMSystemException, TException {
        return noteStore.listNotebooks();
    }

    @Test
    void createNotebookTest() throws TException, EDAMUserException, EDAMSystemException {
        log.info("@createNotebook");
        Notebook notebook = createNotebook();
        log.info("\tnotebook " + notebook.getName() + " create");
    }

    private Notebook createNotebook() throws EDAMUserException, EDAMSystemException, TException {
        Notebook notebook = new Notebook();
        String notebookName = REMOTE_NOTEBOOK_PREFIX + UUID.randomUUID().toString();
        notebook.setName(notebookName);
        notebook = noteStore.createNotebook(notebook);
        return notebook;
    }

    @Test
    void createNote() throws EDAMUserException, EDAMSystemException, EDAMNotFoundException, TException {
        log.info("@createNote");
        String title = REMOTE_NOTE_TITLE_PREFIX + UUID.randomUUID().toString();
        String content = getNoteContent(REMOTE_CONTENT_PREFIX);
        Note note = new Note();
        note.setNotebookGuid(getDefaultNotebook().getGuid());
        note.setTitle(title);
        note.setContent(content);
        noteStore.createNote(note);
        log.info("\tnote " + title + " create successfully");
    }

    private String getNoteContent(String remoteContentPrefix) {
        return DEFAULT_NOTE_CONTENT.replace("${noteBody}", remoteContentPrefix + UUID.randomUUID().toString());
    }

    @Test
    void listNotes() throws EDAMUserException, EDAMSystemException, EDAMNotFoundException, TException {
        log.info("@listNotes");
        Notebook notebook = getDefaultNotebook();
        getNotes(notebook).stream()
                .forEach(n -> System.out.println("\t" + n));

    }

    private List<Note> getNotes(Notebook notebook) {
        try {
            NoteFilter noteFilter = new NoteFilter();
            noteFilter.setNotebookGuid(notebook.getGuid());
            return noteStore.findNotes(noteFilter, 0, Integer.MAX_VALUE).getNotes();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    Notebook getDefaultNotebook() throws EDAMUserException, EDAMSystemException, TException, EDAMNotFoundException {
        log.info("@getDefaultNotebook");
        List<Notebook> notebooks = getNotebooks();
        Optional<Notebook> notebook = notebooks.stream()
                .filter(n -> DEFAULT_NOTEBOOK.equals(n.getName()))
                .findAny();
        log.info("\tnotebook " + notebook + " loaded successfully");
        return notebook.get();
    }

    @Test
    @Order(Integer.MAX_VALUE)
    void removeRemoteNotebooks() throws TException, EDAMUserException, EDAMSystemException, EDAMNotFoundException {
        log.info("@removeRemoteNotebooks");
        List<Notebook> notebooks = getNotebooks().stream()
                .filter(n -> n.getName().startsWith(REMOTE_NOTEBOOK_PREFIX))
                .collect(Collectors.toList());

        for (Notebook notebook : notebooks) {
            noteStore.expungeNotebook(notebook.getGuid());
        }

        notebooks.stream()
                .forEach(n -> System.out.println("\t" + n.getName() + " expunged"));
    }

    @Test
    @Order(Integer.MAX_VALUE)
    void removeRemoteNotes() throws TException, EDAMUserException, EDAMSystemException, EDAMNotFoundException {
        log.info("@removeRemoteNotes");
        List<Note> notes = getNotebooks().stream()
                .flatMap(n -> getNotes(n).stream())
                .filter(n -> n.getTitle().startsWith(REMOTE_NOTE_TITLE_PREFIX))
                .collect(Collectors.toList());

        for (Note note : notes) {
            noteStore.expungeNote(note.getGuid());
        }

        notes.stream()
                .forEach(n -> System.out.println("\t" + n.getTitle() + " expunged"));
    }

    @Test
    void createStack() throws TException, EDAMUserException, EDAMSystemException, EDAMNotFoundException {
        log.info("@createStack");
        String stackName = REMOTE_STACK_PREFIX + UUID.randomUUID().toString();
        Notebook notebook1 = createNotebook();
        Notebook notebook2 = createNotebook();
        Notebook notebook3 = createNotebook();

        notebook1.setStack(stackName);
        notebook2.setStack(stackName);
        notebook3.setStack(stackName);

        noteStore.updateNotebook(notebook1);
        noteStore.updateNotebook(notebook2);
        noteStore.updateNotebook(notebook3);
    }
}