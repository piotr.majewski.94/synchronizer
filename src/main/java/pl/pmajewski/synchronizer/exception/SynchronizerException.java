package pl.pmajewski.synchronizer.exception;

public abstract class SynchronizerException extends RuntimeException {

    private int code = 0;
    private String name = "INTERNAL_SYNCHRONIZER_EXCEPTION";

    protected SynchronizerException(int code, String name) {
        this.code = code;
        this.name = name;
    }

    protected SynchronizerException(int code, String name, Throwable cause) {
        super(cause);
        this.code = code;
        this.name = name;
    }
}
