package pl.pmajewski.synchronizer.evernote.client;

import com.evernote.auth.EvernoteAuth;
import com.evernote.auth.EvernoteService;
import com.evernote.clients.ClientFactory;
import com.evernote.clients.NoteStoreClient;
import com.evernote.edam.error.EDAMSystemException;
import com.evernote.edam.error.EDAMUserException;
import com.evernote.thrift.TException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import pl.pmajewski.synchronizer.evernote.exception.EvernoteClientException;

@Component
public class EvernoteNoteStoreClient {

    private static final String SANDBOX_ENV_NAME = "SANDBOX";

    private EvernoteService evernoteService;

    public EvernoteNoteStoreClient(@Value("${evernote.environment:SANDBOX}") String environment) {
        if(SANDBOX_ENV_NAME.equals(environment)) {
            this.evernoteService = EvernoteService.SANDBOX;
        } else {
            this.evernoteService = EvernoteService.PRODUCTION;
        }
    }

    public NoteStoreClient get(String token) {
        try {
            var auth = new EvernoteAuth(evernoteService, token);
            var clientFactory = new ClientFactory(auth);
            return clientFactory.createNoteStoreClient();
        } catch (EDAMUserException | EDAMSystemException | TException e) {
            throw new EvernoteClientException(e);
        }
    }
}
