package pl.pmajewski.synchronizer.evernote.service;

import com.evernote.edam.type.Note;
import com.evernote.edam.type.Notebook;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.pmajewski.synchronizer.evernote.exception.NotebookNotFoundException;
import pl.pmajewski.synchronizer.evernote.repository.EvernoteRemoteRepository;
import pl.pmajewski.synchronizer.evernote.service.model.EvernoteBookmark;
import pl.pmajewski.synchronizer.evernote.utils.EvernoteENMLUtils;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

@Service
@AllArgsConstructor
public class EvernoteServiceImpl implements EvernoteService {

    private static final String RESOURCE_NOTEBOOK_NAME = "Bookmarks";

    private EvernoteRemoteRepository repository;

    @Override
    public void save(EvernoteBookmark payload) {
        Notebook notebook = getBookmarksNotebook();
        Note note = findNoteInNotebook(notebook)
                .orElseGet(() -> createNote(notebook));
        EvernoteENMLUtils.addTodoListItem(note.getContent());

        repository.update(note);
    }

    private Note createNote(Notebook notebook) {
        Note note = new Note();
        note.setNotebookGuid(notebook.getGuid());
        note.setTitle(getTitlePattern(LocalDate.now()));
        note.setContent(EvernoteENMLUtils.getEmptyNoteBody());

        return repository.create(note);
    }

    private Optional<Note> findNoteInNotebook(Notebook notebook) {
        return repository.listNotes(notebook.getGuid())
                .stream()
                .filter(i -> Instant.ofEpochMilli(i.getCreated()).atZone(ZoneId.systemDefault()).toLocalDate().isEqual(LocalDate.now()))
                .filter(i -> i.getTitle().equals(getTitlePattern(LocalDate.now())))
                .findFirst()
                .map(n -> repository.getNote(n.getGuid()))
                .stream()
                .findFirst();
    }

    private Notebook getBookmarksNotebook() {
        return repository.listNotebooks().stream()
                .filter(i -> i.getName().equals(RESOURCE_NOTEBOOK_NAME))
                .findFirst()
                .orElseThrow(() -> new NotebookNotFoundException(RESOURCE_NOTEBOOK_NAME));
    }

    private String getTitlePattern(LocalDate date) {
        return date.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")) + " | Resources";
    }

}
