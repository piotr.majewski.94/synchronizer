package pl.pmajewski.synchronizer.evernote.service.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class EvernoteBookmark {

    private String title;
    private String url;
}
