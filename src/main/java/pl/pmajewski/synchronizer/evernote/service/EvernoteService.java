package pl.pmajewski.synchronizer.evernote.service;

import pl.pmajewski.synchronizer.evernote.service.model.EvernoteBookmark;

public interface EvernoteService {

    void save(EvernoteBookmark note);
}
