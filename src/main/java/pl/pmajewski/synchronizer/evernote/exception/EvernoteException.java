package pl.pmajewski.synchronizer.evernote.exception;

import pl.pmajewski.synchronizer.exception.SynchronizerException;

public class EvernoteException extends SynchronizerException {

    public EvernoteException(Throwable cause) {
        super(200, "EVERNOTE_INTERNAL_EXCEPTION", cause);
    }

    protected EvernoteException(int code, String name, Throwable cause) {
        super(code, name, cause);
    }
}
