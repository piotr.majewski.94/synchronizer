package pl.pmajewski.synchronizer.evernote.exception;

public class NotebookNotFoundException extends RuntimeException {

    public NotebookNotFoundException(String notebookName) {
        super("Notebook with name="+notebookName+" not found");
    }
}
