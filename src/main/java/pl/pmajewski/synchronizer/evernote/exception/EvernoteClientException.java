package pl.pmajewski.synchronizer.evernote.exception;

public class EvernoteClientException extends EvernoteException {

    public EvernoteClientException(Throwable cause) {
        super(201, "EVERNOTE_CLIENT_EXCEPTION", cause);
    }
}
